<?php

namespace Tiny96\Laravel\QueryFilter;

use Illuminate\Container\Container;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;

abstract class QueryFilter
{

    /** @var Request */
    protected $request;
    protected $persistence_key = null;

    protected $prefixes = [];
    protected $filters = [];
    protected $sorters = [];
    protected $queries = [];
    protected $withs = [];
    protected $withCounts = [];
    protected $ignore = [];

    protected $filterKeys = ['f'];
    protected $queryKeys = ['q'];
    protected $sorterKeys = ['s'];

    protected $strictMap = true;
    protected $filterKeyMap = [];
    protected $prefixKeyMap = [];
    protected $sorterKeyMap = [];

    protected $defaultFilters = [];
    protected $defaultSorters = [];
    protected $use_default = true;

    protected $joined = [];

    protected $useIds = [];

    protected $model;

    /**
     * QueryFilter constructor.
     * Override this and pass your model here
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        if (is_null($this->persistance_key)) {
            $this->persistance_key = sha1(static::class);
        }
        $this->reloadFilters()->request(Container::getInstance()->make('request'));
        $this->model = $model;
        $this->appendConcernMaps();
    }

    public function request(Request $request)
    {
        $this->request = $request;
        $this->extractFilters($request);

        return $this;
    }

    public function reset()
    {
        $this->filters = [];
        $this->sorters = [];
        $this->queries = [];
        $this->useIds = [];

        return $this;
    }

    public function persistence($key)
    {
        $this->persistence_key = $key;

        return $this;
    }

    /**
     * @param array $values
     *
     * @return $this
     */
    public function ids($values)
    {
        if (is_array($values) and $values = array_filter($values)) {
            $this->useIds = $values;
        }

        return $this;
    }

    /**
     * @param array $filters
     *
     * @return $this
     */
    public function defaultFilters($filters = [])
    {
        $this->defaultFilters = is_array($filters) ? $filters : [$filters];

        return $this;
    }

    /**
     * @param array $sorters
     *
     * @return $this
     */
    public function defaultSorters($sorters = [])
    {
        $this->defaultSorters = is_array($sorters) ? $sorters : [$sorters];

        return $this;
    }

    /**
     * @param string|array|null $key
     * @param string|null       $value
     *
     * @return $this
     */
    public function always($key, $value = null)
    {
        if (is_array($key)) {
            foreach ($key as $k => $v) {
                if (is_string($v)) {
                    $v = trim($v);
                }
                if ($v) {
                    $this->filters[$k] = $v;
                }
            }
        } else {
            if (is_string($value)) {
                $value = trim($value);
            }
            if ($value) {
                $this->filters[$key] = $value;
            }
        }

        return $this;
    }

    /**
     * @param Collection|Arrayable|array $data
     *
     * @return void
     */
    protected function extractFilters($data = null)
    {
        if (is_null($data)) {
            $data = Container::getInstance()->make('request');
        }
        $filters = $this->parseKeysFromData($this->filterKeys, collect($data), function($node) {
            if ($this->isPrefixed($node) and !$this->isValidPrefixed($node)) {
                return '';
            }

            return $node;
        });

        $sorters = $this->parseKeysFromData($this->sorterKeys, $data);
        $queries = $this->parseKeysFromData($this->queryKeys, $data);
        $this->use_default = empty($filters) && empty($queries);

        if (array_has($data,'reset')) {
            $filters = array_merge($this->filters, $filters);
            $sorters = array_merge($this->sorters, $sorters);
            $queries = array_merge($this->queries, $queries);
        }

        $this->filters = array_filter($filters);
        $this->sorters = array_filter($sorters);
        $this->queries = array_filter($queries);
    }

    protected function parseKeysFromData($keys, $data, $callback = null)
    {
        $parsed = [];
        if (is_array($data)) {
            $data = collect($data);
        }
        if ($data instanceof Request || $data instanceof Collection) {
            foreach ($keys as $key) {
                if ($data->has($key) and $value = $data->get($key)) {
                    if (!is_array($value)) {
                        $value = [$value];
                    }
                    foreach ($value as $nodeKey => $node) {
                        if (is_string($node)) {
                            $node = $this->parseQueryText($node);
                        }
                        if (is_string($nodeKey)) {
                            $node = collect($node)->map(is_callable($callback) ? $callback : function($cell) {
                                return is_string($cell) ? trim($cell) : $cell;
                            })->filter();
                            if ($node->count() > 1) {
                                $parsed[$nodeKey] = $node->toArray();
                            } else {
                                $parsed[$nodeKey] = $node->first();
                            }
                        } else {
                            $node = collect($node)->map(is_callable($callback) ? $callback : function($cell) {
                                return is_string($cell) ? trim($cell) : $cell;
                            })->filter();
                            if ($node->count() > 1) {
                                $parsed[] = $node->toArray();
                            } else {
                                $parsed[] = $node->first();
                            }
                        }
                    }
                }
            }
        }

        $mapped = array_map(function($node) {
            return is_string($node) ? trim($node) : $node;
        }, $parsed);

        return $mapped;
    }

    protected function parseQueryText($text)
    {
        $parsed = [];

        if (is_string($text)) {
            $parsed = preg_split('/[\|]+/', $text);
        }

        return $parsed;
    }

    protected function isPrefixed($text)
    {
        return preg_match('/.+:.+/', $text);
    }

    protected function isValidPrefixed($text)
    {
        if ($this->isPrefixed($text) and $prefix = $this->splitPrefix($text)) {
            return in_array($prefix[0], $this->prefixes);
        }

        return false;
    }

    protected function splitPrefix($text)
    {
        if (preg_match('/([^:]+):(.+)/', $text, $matches)) {
            return [trim($matches[1]), trim($matches[2])];
        }

        return ['', $text];
    }

    /**
     * @param array|Collection $data
     *
     * @return $this
     */
    public function using($data)
    {
        $this->extractFilters($data);

        return $this;
    }

    /**
     * @param string|array|null $key
     *
     * @return $this
     */
    public function ignore($key)
    {
        if (is_null($key)) {
            $this->ignore = [];
        } elseif (is_string($key)) {
            $this->ignore[] = $key;
        } elseif (is_array($key)) {
            $this->ignore = array_unique($this->ignore + $key);
        }

        return $this;
    }

    public function keywordParamList()
    {
        $keywords = [];
        foreach ($this->queries() as $query) {
            $params = [];
            if ($this->getFilterKey()) {
                $node = [];
                foreach ($this->filters() as $key => $value) {
                    $node[$key] = $value;
                }
                if (!empty($node)) {
                    $params[$this->getFilterKey()] = $node;
                }
            }
            if ($this->getSorterKey()) {
                $node = [];
                foreach ($this->sorters as $key => $value) {
                    $node[] = $value;
                }
                if (!empty($node)) {
                    $params[$this->getSorterKey()] = $node;
                }
            }
            if ($this->getQueryKey()) {
                $node = [];
                foreach ($this->queries() as $value) {
                    if ($value !== $query) {
                        $node[] = $value;
                    }
                }
                if (!empty($node)) {
                    $params[$this->getQueryKey()] = $node;
                }
            }
            $keywords[$query] = $params;
        }

        return collect($keywords);
    }

    public function queries()
    {
        return collect($this->queries);
    }

    public function getFilterKey()
    {
        if (is_array($this->filterKeys)) {
            return count($this->filterKeys) ? reset($this->filterKeys) : '';
        }

        return $this->filterKeys;
    }

    public function filter($key = null)
    {
        return $this->filters($key);
    }

    public function filters($key = null)
    {
        if (is_null($key)) {
            return collect($this->filters);
        } elseif (is_string($key)) {
            return isset($this->filters[$key]) ? $this->filters[$key] : '';
        } elseif (is_array($key)) {
            return collect(array_only($this->filters, $key));
        }

        return collect();
    }

    public function outputFilters($key = null)
    {
        $results = $this->filters($key);
        if (is_array($results)) {
            $results = collect($results);
        }
        if ($results instanceof Collection) {
            return $results->implode('|');
        }

        return $results;
    }

    public function getSorterKey()
    {
        if (is_array($this->sorterKeys)) {
            return count($this->sorterKeys) ? reset($this->sorterKeys) : '';
        }

        return $this->sorterKeys;
    }

    public function getQueryKey()
    {
        if (is_array($this->queryKeys)) {
            return count($this->queryKeys) ? reset($this->queryKeys) : '';
        }

        return $this->queryKeys;
    }

    public function isFilterValue($key, $value, $true = true, $false = false)
    {
        if (isset($this->filters[$key])) {
            $value = is_array($value) ? $value : [$value];

            return collect($this->filters[$key])->intersect($value)->isNotEmpty() ? $true : $false;
        }

        return $false;
    }

    public function selectedFilterValue($key, $value, $true = 'selected', $false = '')
    {
        return $this->isFilterValue($key, $value) ? $true : $false;
    }

    public function checkedFilterValue($key, $value, $true = 'checked', $false = '')
    {
        return $this->isFilterValue($key, $value) ? $true : $false;
    }

    public function isSorter($value, $offset = 1)
    {
        $offset--;
        if (!is_numeric($offset) || count($this->sorters) <= $offset) {
            return false;
        }

        return $this->sorters[$offset] == $value;
    }

    /**
     * @param array $columns
     *
     * @return EloquentCollection|static[]
     */
    public function get($columns = ['*'])
    {
        $this->persistFilters();

        return $this->getQuery()->get();
    }

    /**
     * @return EloquentBuilder|QueryBuilder
     */
    protected function getQuery()
    {
        $this->joined = [];
        $query = $this->applySorters($this->applyQueries($this->startQuery()));
        !empty($this->withs) and $this->appendWith($query);
        !empty($this->withCounts) and $this->appendWithCount($query);

        return $query->orderBy('id');
    }

    /**
     * @return EloquentBuilder|QueryBuilder
     */
    public function getBuilder()
    {
        return $this->getQuery();
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     *
     * @return EloquentBuilder|QueryBuilder
     */
    protected function applySorters($query)
    {
        if ($this->sorters()->isEmpty()) {
            $this->sorters = $this->defaultSorters;
        }
        $this->sorters()->each(function($sorter) use ($query) {
            $this->handleSorter($query, $sorter);
        });

        return $query;
    }

    public function sorters()
    {
        return collect($this->sorters);
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @param string                       $sorter
     *
     * @return EloquentBuilder|QueryBuilder
     */
    protected function handleSorter($query, $sorter)
    {
        if ($handler = $this->getSorterHandler($sorter)) {
            $this->{$handler}($query);
        }

        return $query;
    }

    protected function getSorterHandler($sorter)
    {
        $sorter = trim($sorter);
        $asc = true;
        if (ends_with($sorter, ['+', '-'])) {
            $asc = ends_with($sorter, '+');
            $sorter = substr($sorter, 0, -1);
        }
        if (!empty($sorter)) {
            if ($handler = array_get($this->sorterKeyMap, $sorter, $this->strictMap ? null : $sorter)) {
                $handler = 'handle' . studly_case($handler) . ($asc ? '' : 'Desc') . 'Sorter';
                if (method_exists($this, $handler)) {
                    return $handler;
                }
            }
        }

        return null;
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     *
     * @return EloquentBuilder|QueryBuilder
     */
    protected function applyQueries($query)
    {
        if ($this->queries()->isEmpty() && $this->filters()->isEmpty() && $this->use_default) {
            $this->filters = $this->defaultFilters;
        }

        return $query->where(function($query) {
            if (!empty($this->useIds)) {
                $query->whereIn('id', $this->useIds);
            } else {
                $this->queries()->each(function($node) use ($query) {
                    if ($this->isValidPrefixed($node) and $split = $this->splitPrefix($node)) {
                        $this->handlePrefix($query, $split[0], $split[1]);
                    } else {
                        $this->handleLeftover($query, $node);
                    }
                });
                $this->filters()->except($this->ignore)->each(function($value, $key) use ($query) {
                    $this->handleFilter($query, $key, $value);
                });
            }

            return $query;
        });
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @param string                       $key
     * @param string                       $value
     *
     * @return EloquentBuilder|QueryBuilder
     */
    protected function handlePrefix($query, $key, $value)
    {
        if ($handler = $this->getPrefixHandler($key)) {
            $this->{$handler}($query, $value);
        }

        return $query;
    }

    protected function getPrefixHandler($prefix)
    {
        $prefix = trim($prefix);
        if (!empty($prefix)) {
            if ($handler = array_get($this->prefixKeyMap, $prefix, $this->strictMap ? null : $prefix)) {
                $handler = 'handle' . studly_case($handler) . 'Prefix';
                if (method_exists($this, $handler)) {
                    return $handler;
                }
            }
        }

        return null;
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @param string                       $value
     *
     * @return EloquentBuilder|QueryBuilder
     */
    protected function handleLeftover($query, $value)
    {
        return $query;
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @param string                       $key
     * @param string                       $value
     *
     * @return EloquentBuilder|QueryBuilder
     */
    protected function handleFilter($query, $key, $value)
    {
        if ($handler = $this->getFilterHandler($key)) {
            $this->{$handler}($query, $value);
        }

        return $query;
    }

    protected function getFilterHandler($filter)
    {
        $filter = trim($filter);
        if (!empty($filter)) {
            if ($handler = array_get($this->filterKeyMap, $filter, $this->strictMap ? null : $filter)) {
                $handler = 'handle' . studly_case($handler) . 'Filter';
                if (method_exists($this, $handler)) {
                    return $handler;
                }
            }
        }

        return null;
    }

    /**
     * @return EloquentBuilder|QueryBuilder
     */
    protected function startQuery()
    {
        return $this->model->query()->select($this->model->getTable() . '.*');
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     *
     * @return EloquentBuilder|QueryBuilder $query
     */
    protected function appendWith($query)
    {
        return $query->with($this->withs);
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     *
     * @return EloquentBuilder|QueryBuilder $query
     */
    protected function appendWithCount($query)
    {
        return $query->withCount($this->withCounts);
    }

    public function each(callable $callback, $count = 1000)
    {
        return $this->getQuery()->each($callback, $count);
    }

    /**
     * @param string      $column
     * @param string|null $key
     *
     * @return \Illuminate\Support\Collection
     */
    public function pluck($column, $key = null)
    {
        return $this->getQuery()->pluck($column, $key);
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->getQuery()->count();
    }

    /**
     * @param int|null $perPage
     * @param array    $columns
     * @param string   $pageNumber
     * @param null     $page
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 15, $columns = ['*'], $pageNumber = 'page', $page = null)
    {
        $this->persistFilters();

        return $this->getQuery()->paginate($perPage, $columns, $pageNumber, $page)->appends($this->getAppends());
    }

    /**
     * @return array
     */
    public function getAppends()
    {
        return array_filter([$this->getFilterKey() => $this->filters, $this->getQueryKey() => $this->queries, $this->getSorterKey() => $this->sorters]);
    }

    /**
     * @param mixed $relations
     *
     * @return $this
     */
    public function with($relations)
    {
        if (is_string($relations)) {
            $relations = func_get_args();
        }
        $this->withs = $relations;

        return $this;
    }

    public function withCount($relations)
    {
        if (is_string($relations)) {
            $relations = func_get_args();
        }
        $this->withCounts = $relations;

        return $this;
    }

    public function hasFilters($except = [])
    {
        return $this->filters()->except($except)->isNotEmpty() || $this->queries()->isNotEmpty();
    }

    /**
     * @param EloquentBuilder|QueryBuilder $query
     * @param string                       $table
     * @param string                       $name
     * @param string                       $local
     * @param string|null                  $foreign
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function addJoin($query, $table, $name, $local, $foreign)
    {
        if (!in_array($name, $this->joined)) {
            $this->joined[] = $name;
            $query->join("{$table} as {$name}", $this->model->getTable() . ".{$local}", '=', "{$name}.{$foreign}");
        }

        return $query;
    }

    protected function appendConcernMaps()
    {
        $mapping = ['filterKeyMap' => 'FilterKeyMap', 'sorterKeyMap' => 'SorterKeyMap', 'prefixKeyMap' => 'PrefixKeyMap'];
        foreach (class_uses_recursive($this) as $trait) {
            foreach ($mapping as $prop => $suffix) {
                if (method_exists($this, $method = 'append' . class_basename($trait) . $suffix)) {
                    $this->{$method}();
                }
            }
        }
    }

    /**
     * Converts ^ and $ at start / end of strings into likes for DB
     *
     * @param string $like
     *
     * @return string
     */
    protected function makeLike($like)
    {
        $pre = '';
        $post = '';
        if (starts_with($like, ['"', '^'])) {
            $like = substr($like, 1);
        } else {
            $pre = '%';
        }
        if (ends_with($like, ['"', '$'])) {
            $like = substr($like, 0, strlen($like) - 1);
        } else {
            $post = '%';
        }
        if (starts_with($like, '_')) {
            $like = preg_replace('/^[_]+/', ' ', $like);
        }
        if (ends_with($like, '_')) {
            $like = preg_replace('/[_]+$/', ' ', $like);
        }

        return "{$pre}{$like}{$post}";
    }

    public function form_hidden()
    {
        $o = [];
        foreach ($this->getAppends() as $prefix => $nodes) {
            foreach ($nodes as $key => $value) {
                if (is_numeric($key)) {
                    $o[] = '<input type="hidden" name="' . $prefix . '" value="' . e($value) . '">';
                } else {
                    if (is_array($value)) {
                        foreach ($value as $kk => $vv) {
                            if (is_numeric($kk)) {
                                $o[] = '<input type="hidden" name="' . $prefix . '[' . $key . '][]" value="' . e($vv) . '">';
                            } else {
                                $o[] = '<input type="hidden" name="' . $prefix . '[' . $key . '][' . $kk . ']" value="' . e($vv) . '">';
                            }
                        }
                    } else {
                        $o[] = '<input type="hidden" name="' . $prefix . '[' . $key . ']" value="' . e($value) . '">';
                    }
                }
            }
        }

        return new HtmlString(implode("\n", $o));
    }

    public function form_query()
    {
        $o = [];
        foreach ($this->getAppends() as $prefix => $nodes) {
            foreach ($nodes as $key => $value) {
                if (is_numeric($key)) {
                    $o[] = "{$prefix}={$value}";
                } else {
                    $o[] = "{$prefix}[{$key}]={$value}";
                }
            }
        }

        return implode('&', $o);
    }

    protected function getPersistanceKey()
    {
        if (!empty($this->persistance_key) && is_string($this->persistance_key)) {
            return 'queryfilter.' . $this->persistance_key;
        }

        return null;
    }

    public function reloadFilters()
    {
        if ($key = $this->getPersistanceKey() and $data = session($key)) {
            !empty($data) and is_array($data) and $this->using($data);
        }

        return $this;
    }

    protected function persistFilters()
    {
        if ($key = $this->getPersistanceKey()) {
            session()->put($key, $this->getAppends());
        }

        return $this;
    }

}
